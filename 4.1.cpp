#include <iostream>
#include <iomanip>
#include <vector>
using namespace std;

class Dragon {	//Class for Dragon
public:
  Dragon(int nn, int nhp, int nw, double nm){
    n = nn;
    hp = nhp;
    weaponNumber = nw;
    morale = nm;
  }
  int getHp(){
    return hp;
  }
  int getWe(){
    return weaponNumber;
  }
  double getMo(){
    return morale;
  }
private:
  int n, hp;
  int weaponNumber;
  double morale;	//Dragon's special characteristic
};

class Ninja {	//Class for Ninja
public:
  Ninja(int nn, int nhp, int nw1, int nw2 ){
    n = nn;
    hp = nhp;
    weaponNumber1 = nw1;
    weaponNumber2 = nw2;
  }
  int getHp(){
    return hp;
  }
  int getWe1(){
    return weaponNumber1;
  }
  int getWe2(){
    return weaponNumber2;
  }
private:
  int n, hp;
  int weaponNumber1, weaponNumber2;	//Ninja can own two weapens
};

class Iceman {	//Class for Iceman
public:
  Iceman(int nn, int nhp, int nw){
    n = nn;
    hp = nhp;
    weaponNumber = nw;
  }
  int getHp(){
    return hp;
  }
  int getWe(){
    return weaponNumber;
  }
private:
  int n, hp;
  int weaponNumber;
};

class Lion {	//Class for Lion
public:
  Lion(int nn, int nhp, int nl){
    n = nn;
    hp = nhp;
    loyalty = nl;
  }
  int getHp(){
    return hp;
  }
  int getLo(){
    return loyalty;
  }
private:
  int n, hp;
  int loyalty;
};

class Wolf {	//Class for Wolf
public:
  Wolf(int nn, int nhp){
    n = nn;
    hp = nhp;
  }
  int getHp(){
    return hp;
  }
private:
  int n, hp;
};

class Center {	//Class for Center
public:
  Center(int hpn, int nd, int nn, int ni, int nl, int nw){
    hp = hpn;
    dhp = nd;
    nhp = nn;
    ihp = ni;
    lhp = nl;
    whp = nw;
  }
  int ProduceSoldier(int time, int *n, int *k, int *c , string cname);
private:
  int hp, dhp, nhp, ihp, lhp, whp;
  vector<Dragon> dl;
  vector<Ninja> nl;
  vector<Iceman> il;
  vector<Lion> ll;
  vector<Wolf> wl;
};
int Center::ProduceSoldier(int time, int *n, int *k, int *c , string cname)
{
  int tk = *k, tc = *c;
  int Suc = 0;
  string weapenName[4] = {"sword", "bomb", "arrow"};
  (*n)++;
  if(tk == 1){	//Produce a Dragon
    if(hp >= dhp){
      hp -= dhp;
      Dragon temp(*n, dhp, *n % 3, (hp * 1.0) / (dhp * 1.0));
      dl.push_back(temp);
      cout << setfill('0') << setw(3) << time << ' ';
      cout << cname << " dragon " << *n << " born with strength " << temp.getHp() << ", " << dl.size() << " dragon in " << cname << " headquarter" << endl;
      cout << "It has a " << weapenName[temp.getWe()] << ", and its morale is " << setiosflags(ios::fixed) << setprecision(2) << temp.getMo() << endl;
      Suc = 1;
    }
    else
      (*c)--;
    if(cname == "red")
      *k = 3;
    else
      *k = 2;
  }
  if(tk == 2){	//Produce a Ninja
    if(hp >= nhp){
      hp -= nhp;
      Ninja temp(*n, nhp, *n % 3, (*n + 1)%3);
      nl.push_back(temp);
      cout << setfill('0') << setw(3) << time << ' ';
      cout << cname << " ninja " << *n << " born with strength " << temp.getHp() << ", " << nl.size() << " ninja in " << cname << " headquarter" << endl;
      cout << "It has a " << weapenName[temp.getWe1()] << " and a " <<  weapenName[temp.getWe2()] << endl;
      Suc = 1;
    }
    else
      (*c)--;
    if(cname == "red")
      *k = 1;
    else
      *k = 3;
  }
  if(tk == 3){  //Produce a Iceman
    if(hp >= ihp){
      hp -= ihp;
      Iceman temp(*n, ihp, *n % 3);
      il.push_back(temp);
      cout << setfill('0') << setw(3) << time << ' ';
      cout << cname << " iceman " << *n << " born with strength " << temp.getHp() << ", " << il.size() << " iceman in " << cname << " headquarter" << endl;
      cout << "It has a " << weapenName[temp.getWe()] << endl;
      Suc = 1;
    }
    else
      (*c)--;
    if(cname == "red")
      *k = 4;
    else
      *k = 5;
  }
  if(tk == 4){  //Produce a lion
    if(hp >= lhp){
      hp -= lhp;
      Lion temp(*n, lhp, hp);
      ll.push_back(temp);
      cout << setfill('0') << setw(3) << time << ' ';
      cout << cname << " lion " << *n << " born with strength " << temp.getHp() << ", " << ll.size() << " lion in " << cname << " headquarter" << endl;
      cout << "Its loyalty is " << temp.getLo() << endl;
      Suc = 1;
    }
    else
      (*c)--;
    if(cname == "red")
      *k = 5;
    else
      *k = 1;
  }
  if(tk == 5){  //Produce a wolf
    if(hp >= whp){
      hp -= whp;
      Wolf temp(*n, whp);
      wl.push_back(temp);
      cout << setfill('0') << setw(3) << time << ' ';
      cout << cname << " wolf " << *n << " born with strength " << temp.getHp() << ", " << wl.size() << " wolf in " << cname << " headquarter" << endl;
      Suc = 1;
    }
    else
      (*c)--;
    if(cname == "red")
      *k = 2;
    else
      *k = 4;
  }
  return Suc;
}

int main(){
  int m, i, j;
  int thp, td, tn, ti, tl, tw;
  vector<int> hp, dhp, nhp, ihp, lhp, whp;	//Save the initial hp for Center and each soldier
  cin >> m;
  for(i = 0 ; i < m ; i++){
    cin >> thp >> td >> tn >> ti >> tl >> tw;
    hp.push_back(thp);
    dhp.push_back(td);
    nhp.push_back(tn);
    ihp.push_back(ti);
    lhp.push_back(tl);
    whp.push_back(tw);
  }
  for(i = 0 ; i < m ; i++){
    Center RedCenter(hp[i], dhp[i], nhp[i], ihp[i], lhp[i], whp[i]), BlueCenter(hp[i], dhp[i], nhp[i], ihp[i], lhp[i], whp[i]);
    int rC = 5 , bC = 5;	//Whether two centers can produce a soldier or not
				//Set initial value as 5, if there is a kind of soldier can not be produced, the value should minus 1
				//Until the value equal 0, means this center can not produce any kinds of soldier
    int time = 0;	//Set initial time
    int rNum = 0, bNum = 0;	//The number of each center's soldier
    int rSuc = 0, bSuc = 0; //The flag to mark if the center produce a soldier successfully
    int rFail = 0, bFail = 0;	//The flag to mark if the center can not produce any kinds of soldier
    int rk = 3, bk = 4; //Which kind of soldier should center produce
    cout << "Case:" << i+1 << endl;
    while((rC + bC) >= 0){
      rSuc = 0;
      bSuc = 0;
      while(rC > 0 && !rSuc){	//RedCenter produces soldier, the order is iceman、lion、wolf、ninja、dragon
		    rSuc = RedCenter.ProduceSoldier(time, &rNum, &rk, &rC , "red");
      }
      if(rC <= 0){
        rFail++;
        rC--;
      }
      if(rFail > 0){
        cout << setfill('0') << setw(3) << time << " red headquarter stops making warriors" << endl;
        rFail = -1000000;
      }
      while(bC > 0 && !bSuc){	//BlueCenter produces soldier, the order is lion、dragon、ninja、iceman、wolf 
	      bSuc = BlueCenter.ProduceSoldier(time, &bNum, &bk, &bC , "blue");
      }
      if(bC <= 0){
        bFail++;
        bC--;
      }
      if(bFail > 0){
        cout << setfill('0') << setw(3) << time << " blue headquarter stops making warriors" << endl;
        bFail = -1000000;
      }
      time++;
    }
  }
  return 0; 
}
