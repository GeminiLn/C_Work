#include <iostream>
#include<cstring>
using namespace std;
class Stack{
public:
  Stack(int n){
    head = new int[n];
    hsize = n;
    num = 0;
  }
  ~Stack(){
    delete [] head;
  }
  void Push(int n){
    if(num < hsize){
      head[num] = n;
      num ++;
    }
    else
      cout << "Runtime Error:The Stack is full." << endl;
  }
  int Pop(){
    if(num > 0){
      num --;
      return head[num];
    }
    else
      cout << "Runtime Error:The Stack is empty." << endl;
  }
  int Peer(){
    return head[num - 1];
  }
  bool IsEmpty(){
    if(num > 0)
      return false;
    else
      return true;
  }
  bool IsFull(){
    if(num == hsize)
      return true;
    else
      return false;
  }
  int size(){
    return hsize;
  }
  void resize(int n){
    int *newp = new int[n];
    memcpy(newp, head, sizeof(head));
    head = newp;
    hsize = n;
  }
  
private:
  int *head;
  int hsize;
  int num;
};

int main(){
  return 0;
}