#include<iostream>
#include<math.h>
using namespace std;
bool isPrime[30000000];
void FindPrime(int n)
{
	int i, j;
	for(i = 1 ; i <= n ; i++)
		isPrime[i] = false;
	isPrime[1] = true;
	for(i = 2 ; i < n ; i++)
		if(isPrime[i] == true)
			continue;
		else
			for(j = i + i; j < n ; j += i)
				isPrime[j] = true;
}
int main()
{
	int n, np, i, j;
	cout << "Input n should less than 30000000" << endl;
	cin >> n;
	FindPrime(n);
	np = 0;
	for(i = 1 ; i < n ; i++)
		if(isPrime[i] == false && isPrime[n - i] == false){
			cout << i << " + " << n - i << " = " << n << endl;
			break;
		}
	return 0;
}