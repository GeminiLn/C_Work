#include<iostream>
#include<math.h>
using namespace std;
int main()
{
	double a, b , c;
	cin >> a >> b >> c;
	if((a + b) <= c || (a + c) <= b || (b + c) <= a){
		cout << "Can't get a triangle" << endl;
		return 0;
	}
	else{
		double p = (a + b + c) / 2;
		double s;
		s = sqrt(p * (p - a) * (p - b) * (p - c));
		cout << "The area of this triangle is " << s << endl;
	}
	return 0;
}
