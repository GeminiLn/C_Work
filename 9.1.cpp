#include <iostream>
#include <list>
#include <cstring>
#include <map>
using namespace std;

int main() {
    map<int, list<int> > m;
    int n, i, sum = 0;
    string order;
    cin >> n;
    for(i = 1 ; i <= n; i++) {
        cin >> order;
        if(order == "new"){
            int num;
            cin >> num;
            m.insert(pair<int, list<int> >(num, list<int>()));
        }
        else if(order == "add"){
            int num, t;
            cin >> num >> t;
            m[num].push_back(t);
        }
        else if(order == "merge"){
            int num1, num2;
	    cin >> num1 >> num2;
            m[num1].sort();
            m[num2].sort();
            m[num1].merge(m[num2]);
        }
        else if(order == "unique"){
            int num;
	    cin >> num;
            m[num].sort();
            m[num].unique();
        }
        else if(order == "out"){
            int num;
	    cin >> num;
	    cout << endl;
            m[num].sort();
            for(list<int>::iterator ite = m[num].begin(); ite != m[num].end(); ite++)
                cout << *ite << " ";
	    
        }
    }
    return 0;
}
