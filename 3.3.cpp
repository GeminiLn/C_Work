#include <iostream>
using namespace std;

class Date {
public:
  Date(int y, int m, int d);
  ~Date() { };
  Date(const Date &d);
  void getDate(){
    cout << year << "-" << month << "-" << day << endl;
  }
  void inputDate(int y, int m, int d){
    year = y;
    month = m;
    day = d;
  }
private:
  int year;
  int month;
  int day;
};
Date::Date(int y, int m, int d)
{
  year = y;
  month = m;
  day = d;
}
Date::Date(const Date& d)
{
  year = d.year;
  month = d.month;
  day = d.day;
}

class People {
public:
  People() : birthday(1970, 1, 1) { };
  People(int n, string g, int y, int m, int d, string i);
  People(const People &p);
  ~People() { };
  void showPeople(){
    cout << "Number: " << number << endl;
    cout << "Gender: " << gender << endl;
    cout << "Birthday: " ;
    birthday.getDate();
    cout << "ID: " << id << endl << endl;
  }
private:
  int number;
  string gender;
  Date birthday;
  string id;
};
People::People(int n, string g, int y, int m, int d, string i) : birthday(y, m, d)
{
  number = n;
  gender = g;
  id = i;
}
People::People(const People &p) : birthday(p.birthday){
  number = p.number;
  gender = p.gender;
  id = p.id;
}


int main() {
  int i = 1;
  string t;
  cout << "Input 'New' to input a new People,Input 'Show' to show the existing People, Input 'Exit' to end this program" << endl;
  People p[100];
  while(cin >> t){
    if(t == "New"){
      cout << "Input the number,gender,birth year,birth month,birth day,id seperated by spaces (The number should be input by order)" << endl;
      cout << "Such as '1 male 1970 1 1 100000000'" << endl;
	  int n,y,m,d;
      string g,id;
      cin >> n >> g >> y >> m >> d >> id;
      People pt(n, g, y, m, d, id);
      p[i] = pt;
      i++;
    }
    if(t == "Show"){
      cout << "Input the number of people" << endl;
      int n;
      cin >> n;
      p[n].showPeople();
    }
    if(t == "Exit")
      break;
    cout << "Input 'New' to input a new People,Input 'Show' to show the existing People, Input 'Exit' to end this program" << endl;
  }
  return 0;
}
