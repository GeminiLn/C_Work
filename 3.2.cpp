#include <iostream>
using namespace std;

class Cube {
public:
  Cube(double l, double w, double h){ 
    length = l; 
    width = w;
    height = h;
  }
  double getVol(){
    CalVol();
    return vol;
  }
private:
  double length,width,height;
  double vol;
  void CalVol(){
    vol = length * width * height;
  }
};

int main() {
  double l,w,h;
  cout << "Input the length,width and height of the cuboid, saparated by spaces" << endl;
  cin >> l >> w >> h;
  Cube c(l, w, h);
  cout << "The volume of the cuboid is " << c.getVol() << endl;
  return 0;
}