#include<iostream>
#include<string>
#include<math.h>
using namespace std;
int main()
{
	double a, b, c;
	double delta, x, x1, x2;
	cin >> a >> b >> c;
	if(a == 0){
		if(b == 0)
			cout << "No Answer" << endl;
		else{
			x = (-1) * c / b;
			cout << "x = " << x << endl;
		}
	}
	else{
		delta = b * b - 4 * a * c;
		if(delta > 0){
			x1 = ((-1) * b - sqrt(delta)) / (2 * a);
			x2 = ((-1) * b + sqrt(delta)) / (2 * a);
			cout << "There are two answers" << endl << "x1 = " << x1 << endl << "x2 = " << x2 << endl;
		}
		if(delta == 0){
			x = (-1) * b / (2 * a);
			cout << "x = " << x << endl;
		}
		if(delta < 0)
			cout << "No Answer" << endl;
	}
	return 0;
}
