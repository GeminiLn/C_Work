#include<iostream>
#include<stdlib.h>
#include<time.h>
using namespace std;
int main()
{
	int n, k, t1, t2;
	cout << "Input the number of mountains and the upper limit number of moving mountains at once" << endl;
	cin >> n >> k;
	while(n > 0){
		cout << "Input the number of mountains you will move" << endl;
		cin >> t1;
		while(t1 > k){
			cout << "The number is out of limit" << endl << "Input a new number of mountains you will move" << endl;
			cin >> t1; 
		}
		while(t1 > n){
			cout << "The are not enough mountains for you to move" << endl << "Input a new number of mountains you will move" << endl;
			cin >> t1;
		}
		n -= t1;
		if(n == 0){
			cout << "Player win the game" << endl;
			break;
		}
		t2 = rand() % k + 1;
		while(t2 > n)
			t2 = rand() % k + 1;
		cout << "Computer move " << t2 << " mountains" << endl;
		n -= t2;
		if(n == 0){
			cout << "Computer win the game" << endl;
			break;
		}
		cout << "There are " << n << " mountains remained" << endl;
	}
	return 0;
}